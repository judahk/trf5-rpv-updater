﻿using System;
using System.Threading;
using System.IO;
using System.Net.Http;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Web;

namespace TRF5
{
    class Program
    {
        static void Main(string[] args)
        {
            #region declarations
            string rpv, processo, uf;
            SqlDataReader reader;
            int index, limit;
            #endregion

            #region get index & limit
            reader = readme("SELECT log, dountil FROM tb_logs WHERE project = 'TRF5 - RPV Updater'");
            reader.Read();
            index = (int)reader["log"];
            limit = (int)reader["dountil"];
            reader.Close();
            #endregion

            #region loop into trf5
            for (int i = index; i < limit; i++)
            {
                Console.WriteLine("Trying: RPV" + i.ToString() + "\n\n");
                string htmlOutput = run_cmd(((char)34) + Directory.GetCurrentDirectory().Replace("\\", "/") + "/postRequest.py" + ((char)34), i.ToString() + " R");
                htmlOutput = HttpUtility.HtmlDecode(htmlOutput);
                
                using (StringReader sr = new StringReader(htmlOutput))
                {
                    while (sr.Peek() >= 0)
                    {
                        string line = sr.ReadLine();
                        if (line.Contains("RPV" + i.ToString()))
                        {
                            int li;
                            li = line.IndexOf("Processo Nº");
                            processo = line.Substring(li + 12, 25);

                            li = line.IndexOf("RPV" + i.ToString());                            
                            string[] splitted = getter(line.Substring(li + 3, line.Length - (li + 3)), "<", "left").Split('-');

                            rpv = splitted[0];
                            uf = splitted[1];

                            Console.WriteLine("Results:\n");
                            Console.WriteLine("processo: " + processo + " rpv = " + rpv + " uf = " + uf + "\n\n\n");

                            #region export processo, uf, rpv; onerror: export exception 
                            try
                            {
                                execute("INSERT INTO updatative.tb_trf5 (tipo, requisitorio, processo, uf) VALUES ('RPV', '" + rpv + "', '" + processo + "', '" + uf + "')");
                            }
                            catch (Exception e)
                            {
                                execute("UPDATE tb_logs SET exception = '" + e.ToString().Replace("'", "''") + "' WHERE project = 'TRF5 - RPV Updater'");
                                Console.WriteLine("Code Exited at time: " + DateTime.Now.ToString() + "\n\nWith Error:\n" + e.ToString());

                                Thread.Sleep(10000);
                                System.Environment.Exit(1);                           
                            }
                            finally
                            {
                                execute("UPDATE tb_logs SET log = " + i.ToString() + " WHERE project = 'TRF5 - RPV Updater'");
                            }
                            #endregion
                        }
                    }
                }
                #region save log
                execute("UPDATE tb_logs SET log = " + i.ToString() + " WHERE project = 'TRF5 - RPV Updater'");
                #endregion
            }
            #endregion
        }

        static string getter(string entry, string delimiter, string type)
        {            
            int i;
            string x, y;

            x = "1";
            y = "2";
            i = 1;
            while (x != y.Replace(delimiter, ""))
            {
                if (type == "left")
                {
                    x = Left(entry, i);
                    y = Left(entry, i + 1);
                }
                else if (type == "right")
                {
                    x = Right(entry, i);
                    y = Right(entry, i + 1);
                }
                i++;
            }
            return x;
        }

        static string run_cmd(string cmd, string args)
        {
            ProcessStartInfo start = new ProcessStartInfo();

            start.FileName = @"C:\Python27amd64\python.exe";
            start.Arguments = string.Format("{0} {1}", cmd, args);
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            string result = "";
            using (Process process = Process.Start(start))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    result = reader.ReadToEnd();
                }
            }

            return result;
        }

        static SqlDataReader readme(string query)
        {
            #region declarations
            string connetionString;

            SqlConnection cnn;
            SqlCommand command;
            SqlDataReader reader;
            #endregion

            #region open conn
            connetionString = "dbconnectionstring";
            cnn = new SqlConnection(connetionString);
            cnn.Open();
            #endregion

            #region open reader
            command = new SqlCommand(query, cnn);
            reader = command.ExecuteReader();
            #endregion

            return reader;
        }

        static void execute(string query)
        {
            #region declarations
            string connetionString;

            SqlConnection cnn;
            SqlCommand command;
            #endregion

            #region open conn
            connetionString = "dbconnectionstring";
            cnn = new SqlConnection(connetionString);
            cnn.Open();
            #endregion

            #region query & reader
            command = new SqlCommand(query, cnn);
            command.ExecuteNonQuery();
            cnn.Close();
            #endregion
        }

        public static string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        public static string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

        public static string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }
    }
}

